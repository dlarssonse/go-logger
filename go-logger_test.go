package logger

import (
	"testing"
)

func TestErrors(t *testing.T) {
	OutputDebug = true
	OutputInfo = true
	OutputConsole = true
	OutputFilename = "go-logger_test.txt"
	OutputJSON = "go-logger_test.json"
	OutputAsReadable = true
	IgnoredFiles = append(IgnoredFiles, "go-logger_test.go")

	ClearLog()

	Printf("Testing printf message, should be info")
	Debug("Testing debug message")
	Info("Testing info message")
	Warning("Testing warning message")
	Error("Testing error message")
	Fatalf("Testing fatal message")

	Info(struct{ Object string }{Object: "123"})
}
